<?php

class Insurance_Calculator_Block_Adminhtml_Sales_CreditmemoTotals extends Mage_Sales_Block_Order_Creditmemo_Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();

        Mage::helper('insurance_calculator')->addInsuranceTotals($this);

        return $this;
    }
}
