<?php

class Insurance_Calculator_Block_Adminhtml_Sales_InvoiceTotals extends Mage_Sales_Block_Order_Invoice_Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();

        Mage::helper('insurance_calculator')->addInsuranceTotals($this);

        return $this;
    }
}
