<?php

class Insurance_Calculator_Block_Sales_InvoiceTotals extends Mage_Sales_Block_Order_Invoice_Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();

        Mage::helper('insurance_calculator')->addInsuranceShippingTotals($this);

        return $this;
    }
}