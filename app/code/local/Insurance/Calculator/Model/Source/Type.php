<?php


class Insurance_Calculator_Model_Source_Type
{
    const TYPE_ABSOLUTE = 1;
    const TYPE_ABSOLUTE_LABEL = 'Absolute';

    const TYPE_PERCENT = 2;
    const TYPE_PERCENT_LABEL = 'Percentage';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        foreach ($this->toArray() as $value => $label) {
            $options[] = array('value' => $value, 'label' => $label);
        }
        return $options;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            self::TYPE_ABSOLUTE => self::TYPE_ABSOLUTE_LABEL,
            self::TYPE_PERCENT => self::TYPE_PERCENT_LABEL,
        );
    }
}